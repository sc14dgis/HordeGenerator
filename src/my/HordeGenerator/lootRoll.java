/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;
import java.util.Random;
/**
 *
 * @author Daniel
 */
public class lootRoll {
    int challenge = -1;
    int roll = -1;
    
    int diceRoll(int die){
        Random dieRand = new Random();
        int dieRoll = 0;
        if (die == 4){
            dieRoll = dieRand.nextInt(die) + 1;
        }
        return dieRoll;
    }
    
    lootRoll(){
        Random rand = new Random();
        roll = rand.nextInt(100) + 1;
    }
    
    String getLoot(int lootCR){
        if (lootCR == 4){
            CR4 loot = new CR4();
            return loot.returnLoot();
        }else if(lootCR ==10){
            CR10 loot = new CR10();
            return loot.returnLoot();
        }else if(lootCR ==16){
            CR16 loot = new CR16();
            return loot.returnLoot();
        }else {
            CR17P loot = new CR17P();
            return loot.returnLoot();
        }
    }
}
