/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableA {
    
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem = "";
        
        if (diceRoll <=50){
            lootItem = "Potion of healing\n";
        }else if (diceRoll <= 60){
            lootItem = ("SpellScroll(Cantrip)\n");
        }else if (diceRoll <= 70){
            lootItem = ("Potion of climbing\n");
        }else if (diceRoll <= 90){
            lootItem = ("SpellScroll(1st level)\n");
        }else if (diceRoll <= 94){
            lootItem = ("SpellScroll(2nd level)\n");
        }else if (diceRoll <= 98){
            lootItem = ("SpellScroll(Cantrip)\n");
        }else if (diceRoll <= 60){
            lootItem = ("Potion of greater healing\n");
        }else if (diceRoll <= 99){
            lootItem = ("Bag of Holding\n");
        }else{
            lootItem = ("Driftglobe\n");
        }
        
        
        return lootItem;
    }
    
}
