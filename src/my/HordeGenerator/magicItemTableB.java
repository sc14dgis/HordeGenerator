/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableB {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem = "";
        
        if (diceRoll <=15){
            lootItem = "Potion of greater healing\n";
        }else if (diceRoll <= 22){
            lootItem = ("Potion of fire breath\n");
        }else if (diceRoll <= 29){
            lootItem = ("Potion of resistance\n");
        }else if (diceRoll <= 34){
            lootItem = ("Amunition, +1\n");
        }else if (diceRoll <= 39){
            lootItem = ("potion of animal friendship\n");
        }else if (diceRoll <= 44){
            lootItem = ("Potion of hill giant strength\n");
        }else if (diceRoll <= 49){
            lootItem = ("Potion of growth\n");
        }else if (diceRoll <= 54){
            lootItem = ("Potion of water breathing\n");
        }else if (diceRoll <= 59){
            lootItem = ("Spell scroll(2nd level)\n");
        }else if (diceRoll <= 64){
            lootItem = ("Spell scroll(level 3)\n");
        }else if (diceRoll <= 67){
            lootItem = ("Bag of holding\n");
        }else if (diceRoll <= 70){
            lootItem = ("Keoghtom's  ointment\n");
        }else if (diceRoll <= 73){
            lootItem = ("Oil  of  slipperiness\n");
        }else if (diceRoll <= 75){
            lootItem = ("Dust of disappearance\n");
        }else if (diceRoll <= 77){
            lootItem = ("Dust of dryness\n");
        }else if (diceRoll <= 79){
            lootItem = ("Dust of sneezing and choking\n");
        }else if (diceRoll <= 81){
            lootItem = ("Dust of dryness\n");
        }else if (diceRoll <= 83){
            lootItem = ("Elemental gem\n");
        }else if (diceRoll == 84){
            lootItem = ("Alchemy jug\n");
        }else if (diceRoll == 85){
            lootItem = ("Cap of water breathing\n");
        }else if (diceRoll == 86){
            lootItem = ("Cloak of the manta ray\n");
        }else if (diceRoll == 87){
            lootItem = ("Driftglobe\n");
        }else if (diceRoll == 88){
            lootItem = ("Goggles of night\n");
        }else if (diceRoll == 89){
            lootItem = ("Helm of comprehending languages\n");
        }else if (diceRoll == 90){
            lootItem = ("Immoveable rod\n");
        }else if (diceRoll == 91){
            lootItem = ("Lantern of revealing languages\n");
        }else if (diceRoll == 92){
            lootItem = ("Mariner's armour\n");
        }else if (diceRoll == 93){
            lootItem = ("Mithral armour\n");
        }else if (diceRoll == 94){
            lootItem = ("A Potion of poison\n");
        }else if (diceRoll == 95){
            lootItem = ("Ring of swimming\n");
        }else if (diceRoll == 96){
            lootItem = ("Dust of dissappearance\n");
        }else if (diceRoll == 97){
            lootItem = ("Robe of climbing\n");
        }else if (diceRoll == 98){
            lootItem = ("Saddle of the cavalier\n");
        }else if (diceRoll == 99){
            lootItem = ("Wand of magic detection\n");
        }else{
            lootItem = ("Wand of secrets\n");
        }
        
        
        return lootItem;
    }
}
