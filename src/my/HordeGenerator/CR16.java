/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class CR16 {
    String returnLoot(){
        diceRoller diceRoller = new diceRoller();
        int roll = diceRoller.rollDie(100);
        
        int GP = 0;
        for(int i=0; i<4; i++){GP = GP + diceRoller.rollDie(6); }
        GP = GP*1000;
        
        int PP = 0;
        for(int i=0; i<5; i++){PP = PP + diceRoller.rollDie(6); }
        PP = PP*100;
        
        String itemString = "";
        if (roll <=3){
            // Nothing here unlucky
            itemString = "No items\n";
        }else if(roll <=6){
            GP = GP+1250;
            itemString = "No items\n";
        }else if(roll <=9){
            GP = GP+3750;
            itemString = "No items\n";
        }else if(roll <=12){
            GP = GP+5000;
            itemString = "No items\n";
        }else if(roll <=15){
            GP = GP+10000;
            itemString = "No items\n";
        }else if(roll <=19){
            GP = GP+1250;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableA.getLoot();}
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=23){
            GP = GP+3750;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableA.getLoot();}
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=26){
            GP = GP+5000;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableA.getLoot();}
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=29){
            GP = GP+10000;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableA.getLoot();}
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=35){
            GP = GP+1250;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=40){
            GP = GP+3750;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=45){
            GP = GP+5000;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=50){
            GP = GP+10000;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=54){
            GP = GP+120;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=58){
            GP = GP+3750;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=62){
            GP = GP+5000;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=66){
            GP = GP+10000;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=68){
            GP = GP+1250;
            magicItemTableE tableE = new magicItemTableE();
            itemString = itemString + tableE.getLoot();
        }else if(roll <=70){
            GP = GP+3750;
            magicItemTableE tableE = new magicItemTableE();
            itemString = itemString + tableE.getLoot();
        }else if(roll <=72){
            GP = GP+5000;
            magicItemTableE tableE = new magicItemTableE();
            itemString = itemString + tableE.getLoot();
        }else if(roll <=74){
            GP = GP+10000;
            magicItemTableE tableE = new magicItemTableE();
            itemString = itemString + tableE.getLoot();
        }else if(roll <=76){
            GP = GP+1250;
            magicItemTableF tableF = new magicItemTableF();
            itemString = itemString + tableF.getLoot();
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=78){
            GP = GP+3750;
            magicItemTableF tableF = new magicItemTableF();
            itemString = itemString + tableF.getLoot();
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=80){
            GP = GP+5000;
            magicItemTableF tableF = new magicItemTableF();
            itemString = itemString + tableF.getLoot();
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=82){
            GP = GP+10000;
            magicItemTableF tableF = new magicItemTableF();
            itemString = itemString + tableF.getLoot();
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=85){
            GP = GP+1250;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=88){
            GP = GP+3750;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=90){
            GP = GP+5000;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=92){
            GP = GP+10000;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=94){
            GP = GP+1250;
            magicItemTableI tableI = new magicItemTableI();
            itemString = itemString + tableI.getLoot();
        }else if(roll <=96){
            GP = GP+3750;
            magicItemTableI tableI = new magicItemTableI();
            itemString = itemString + tableI.getLoot();
        }else if(roll <=98){
            GP = GP+5000;
            magicItemTableI tableI = new magicItemTableI();
            itemString = itemString + tableI.getLoot();
        }else {
            GP = GP+10000;
            magicItemTableI tableI = new magicItemTableI();
            itemString = itemString + tableI.getLoot();
        }
        
        String moneyString = GP+"g  "+ PP+"p\n";
        String lootString = moneyString+itemString;
        return lootString;
    }
}
