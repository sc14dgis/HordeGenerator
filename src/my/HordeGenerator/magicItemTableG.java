/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableG {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem;
        
        if (diceRoll <=11){
            lootItem = "Weapon, +2\n";
        }else if (diceRoll <= 14){
            String itemString;
            switch (diceRoller.rollDie(8)){
                case 1  :   itemString = "Bronze griffon";
                        break;
                case 2  :   itemString = "Ebony fly" ;
                        break;
                case 3  :   itemString = "Golden lions" ;
                        break;
                case 4  :   itemString = "Ivory goats" ;
                        break;
                case 5  :   itemString = "EMarble elephant" ;
                        break;
                case 6  :   itemString = "Onyx dog" ;
                        break;
                case 7  :   itemString = "Onyx dog" ;
                        break;
                default: itemString = "Serpentine owl";
                        break;
            }
            lootItem = ("Figureine of wondrous power "+ itemString +" \n");
        }else if (diceRoll <= 15){
            lootItem = ("Adamantine armor (breastplate)\n");
        }else if (diceRoll <= 16){
            lootItem = ("Adamantine armor (splint)\n");
        }else if (diceRoll <= 17){
            lootItem = ("Amulet of health\n");
        }else if (diceRoll <= 18){
            lootItem = ("Armor of vulnerability\n");
        }else if (diceRoll <= 19){
            lootItem = ("Arrow catching shield\n");
        }else if (diceRoll <= 20){
            lootItem = ("Belt of dwarvenkind\n");
        }else if (diceRoll <= 21){
            lootItem = ("Belt of hill giant strength\n");
        }else if (diceRoll <= 22){
            lootItem = ("Berserker axe\n");
        }else if (diceRoll <= 23){
            lootItem = ("Boots of levitation\n");
        }else if (diceRoll <= 24){
            lootItem = ("Boots of speed\n");
        }else if (diceRoll <= 25){
            lootItem = ("Bowl of commanding water elementals\n");
        }else if (diceRoll <= 26){
            lootItem = ("Bracers of defense\n");
        }else if (diceRoll <= 27){
            lootItem = ("Brazier of commanding fire elementals\n");
        }else if (diceRoll <= 28){
            lootItem = ("Cape of mountebank\n");
        }else if (diceRoll <= 29){
            lootItem = ("Censer of controlling air elementals\n");
        }else if (diceRoll <= 30){
            lootItem = ("Armor, +1 chain mail\n");
        }else if (diceRoll <= 31){
            lootItem = ("Armor of resistance (chain mail)\n");
        }else if (diceRoll <= 32){
            lootItem = ("Armor, +1 chain shirt)\n");
        }else if (diceRoll <= 33){
            lootItem = ("Armor of resistance (chain shirt)\n");
        }else if (diceRoll <= 34){
            lootItem = ("Cloak of displacement\n");
        }else if (diceRoll <= 35){
            lootItem = ("Cloak of the bat\n");
        }else if (diceRoll <= 36){
            lootItem = ("cube of force\n");
        }else if (diceRoll <= 37){
            lootItem = ("Daern's instant fortress\n");
        }else if (diceRoll <= 38){
            lootItem = ("Dagger  of  venom\n");
        }else if (diceRoll <= 39){
            lootItem = ("Dimensional shackles\n");
        }else if (diceRoll <= 40){
            lootItem = ("Dragon slayer\n");
        }else if (diceRoll <= 41){
            lootItem = ("Elven  chain \n");
        }else if (diceRoll <= 42){
            lootItem = ("Flame tongue  \n");
        }else if (diceRoll <= 43){
            lootItem = ("Gem  of  seeing\n");
        }else if (diceRoll <= 44){
            lootItem = ("Giant slayer\n");
        }else if (diceRoll <= 45){
            lootItem = ("Clamoured studded leather\n");
        }else if (diceRoll <= 46){
            lootItem = ("Helm  of  teleportation \n");
        }else if (diceRoll <= 47){
            lootItem = ("Horn  of  blasting \n");
        }else if (diceRoll <= 48){
            lootItem = ("Horn  of  Valhalla (silver or brass)\n");
        }else if (diceRoll <= 49){
            lootItem = ("Instrument  of  the bards (Canaith mandolin)\n");
        }else if (diceRoll <= 50){
            lootItem = ("Instrument  ofthe  bards  (Cii  lyre)\n");
        }else if (diceRoll <= 51){
            lootItem = ("loun stone (awareness) \n");
        }else if (diceRoll <= 52){
            lootItem = ("loun stone (protection) \n");
        }else if (diceRoll <= 53){
            lootItem = ("loun  stone  (reserve)\n");
        }else if (diceRoll <= 54){
            lootItem = ("loun  stone  (sustenance)\n");
        }else if (diceRoll <= 55){
            lootItem = ("Iron  bands  of  Bilarro\n");
        }else if (diceRoll <= 56){
            lootItem = ("Armor, + 1 leather\n");
        }else if (diceRoll <= 57){
            lootItem = ("Armor  of  resistance (leather)\n");
        }else if (diceRoll <= 58){
            lootItem = ("Mace  of  disruption\n");
        }else if (diceRoll <= 59){
            lootItem = ("Mace  of  smiting\n");
        }else if (diceRoll <= 60){
            lootItem = ("Mace  of  terror\n");
        }else if (diceRoll <= 61){
            lootItem = ("Mantle  of  spell resistance\n");
        }else if (diceRoll <= 62){
            lootItem = ("Necklace  of  prayer beads\n");
        }else if (diceRoll <= 63){
            lootItem = ("Peri apt  of  proof against poison\n");
        }else if (diceRoll <= 64){
            lootItem = ("Ring  of  animal influence\n");
        }else if (diceRoll <= 65){
            lootItem = ("Ring  of  evasion\n");
        }else if (diceRoll <= 66){
            lootItem = ("Ring  of  feather falling\n");
        }else if (diceRoll <= 67){
            lootItem = ("Ring  of  free action\n");
        }else if (diceRoll <= 68){
            lootItem = ("Ring  of  protection\n");
        }else if (diceRoll <= 69){
            lootItem = ("Ring  of resistance\n");
        }else if (diceRoll <= 70){
            lootItem = ("Ring  of  spell storing\n");
        }else if (diceRoll <= 71){
            lootItem = ("Ring  of  the ram\n");
        }else if (diceRoll <= 72){
            lootItem = ("Ring  of  X-ray  vision\n");
        }else if (diceRoll <= 73){
            lootItem = ("Robe  of  eyes\n");
        }else if (diceRoll <= 74){
            lootItem = ("Rod  of  rulership\n");
        }else if (diceRoll <= 75){
            lootItem = ("Rod  of  the pact keeper,  +2\n");
        }else if (diceRoll <= 76){
            lootItem = ("Rope  of  entanglement\n");
        }else if (diceRoll <= 77){
            lootItem = ("Armor,  +1  scale  mail\n");
        }else if (diceRoll <= 78){
            lootItem = ("Armor  of  resistance (scale mail)\n");
        }else if (diceRoll <= 79){
            lootItem = ("Shield,  +2\n");
        }else if (diceRoll <= 80){
            lootItem = ("Shield  of  missile attraction\n");
        }else if (diceRoll <= 81){
            lootItem = ("Staff  of  charming\n");
        }else if (diceRoll <= 82){
            lootItem = ("Staff  of  healing\n");
        }else if (diceRoll <= 83){
            lootItem = ("Staff  of  swarming insects\n");
        }else if (diceRoll <= 84){
            lootItem = ("Staff  of  the woodlands\n");
        }else if (diceRoll <= 85){
            lootItem = ("Staff  of  withering\n");
        }else if (diceRoll <= 86){
            lootItem = ("Stone  of  controlling earth elementals\n");
        }else if (diceRoll <= 87){
            lootItem = ("Sun blade\n");
        }else if (diceRoll <= 88){
            lootItem = ("Sword  of  life  stealing\n");
        }else if (diceRoll <= 89){
            lootItem = ("Sword  of  wounding\n");
        }else if (diceRoll <= 90){
            lootItem = ("Tentacle rod\n");
        }else if (diceRoll <= 91){
            lootItem = ("Vicious weapon\n");
        }else if (diceRoll <= 92){
            lootItem = ("Wand  of  binding\n");
        }else if (diceRoll <= 93){
            lootItem = ("Wand  of  enemy detection\n");
        }else if (diceRoll <= 94){
            lootItem = ("Wand  of  fear\n");
        }else if (diceRoll <= 95){
            lootItem = ("Wand  of  fireballs\n");
        }else if (diceRoll <= 96){
            lootItem = ("Wand  of  lightning bolts\n");
        }else if (diceRoll <= 97){
            lootItem = ("Wand  of  paralysis\n");
        }else if (diceRoll <= 98){
            lootItem = ("Wand  of  the war mage,  +2\n");
        }else if (diceRoll <= 99){
            lootItem = ("Wand  of  wonder\n");
        }else{
            lootItem = ("Wings  of  flying\n");
        }
        
        
        return lootItem;
    }
}
