/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

import java.util.Random;

/**
 *
 * @author Daniel
 */
public class CR4 {
    
    String returnLoot(){
        diceRoller diceRoller = new diceRoller();
        int roll = diceRoller.rollDie(100);
        int CP = 0;
        for(int i=0; i<6; i++){ 
            CP = CP + diceRoller.rollDie(6); 
        }
        CP = CP*100;
                
        int SP = 0;
        for(int i=0; i<3; i++){SP = SP + diceRoller.rollDie(6); }
        SP = SP*100;
        
        int GP = 0;
        for(int i=0; i<3; i++){GP = GP + diceRoller.rollDie(6); }
        GP = GP*10;
        
        
        String itemString = "";
        if (roll <=6){
            // Nothing here unlucky
        }else if(roll <=16){
            GP = GP+70;
            itemString = "No items\n";
        }else if(roll <=26){
            GP = GP+125;
            itemString = "No items\n";
        }else if(roll <=36){
            GP = GP+350;
            itemString = "No items\n";
        }else if(roll <=44){
            GP = GP+70;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableA.getLoot();}
        }else if(roll <=52){
            GP = GP+125;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableA.getLoot();}
        }else if(roll <=60){
            GP = GP+350;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableA.getLoot();}
        }else if(roll <=65){
            GP = GP+70;
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=70){
            GP = GP+125;
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=75){
            GP = GP+350;
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=78){
            GP = GP+125;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=80){
            GP = GP+125;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=85){
            GP = GP+350;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=92){
            GP = GP+125;
            magicItemTableF tableF = new magicItemTableF();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableF.getLoot();}
        }else if(roll <=97){
            GP = GP+350;
            magicItemTableF tableF = new magicItemTableF();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableF.getLoot();}
        }else if (roll <=99){
            GP = GP+125;
            magicItemTableG tableG = new magicItemTableG();
            itemString = itemString + tableG.getLoot();
        }else {
            GP = GP+350;
            magicItemTableG tableG = new magicItemTableG();
            itemString = itemString + tableG.getLoot();
        }
        
        String moneyString = CP+"c  "+ SP+"s  "+GP+"g\n";
        String lootString = moneyString+itemString;
        return lootString;
    }
}
