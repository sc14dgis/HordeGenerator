/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableF {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem = "";
        
        if (diceRoll <=15){
            lootItem = "Weapon, +1\n";
        }else if (diceRoll <= 18){
            lootItem = ("Shield, +1\n");
        }else if (diceRoll <= 21){
            lootItem = ("Sentinel shield\n");
        }else if (diceRoll <= 23){
            lootItem = ("Amulet of proof against detection and location\n");
        }else if (diceRoll <= 25){
            lootItem = ("Books of elvenkind\n");
        }else if (diceRoll <= 27){
            lootItem = ("Boots of striding and springing\n");
        }else if (diceRoll <= 29){
            lootItem = ("Bracers of archery\n");
        }else if (diceRoll <= 31){
            lootItem = ("Brooch of shielding\n");
        }else if (diceRoll <= 33){
            lootItem = ("Broom of flying\n");
        }else if (diceRoll <= 35){
            lootItem = ("Cloak of elvenkind\n");
        }else if (diceRoll <= 37){
            lootItem = ("Cloak of protection\n");
        }else if (diceRoll <= 39){
            lootItem = ("Gauncletts of ogre power\n");
        }else if (diceRoll <= 41){
            lootItem = ("Hat of disguise\n");
        }else if (diceRoll <= 43){
            lootItem = ("Javelin of lightning\n");
        }else if (diceRoll <= 45){
            lootItem = ("Pearl of power\n");
        }else if (diceRoll <= 47){
            lootItem = ("Rod of the pact keeper, +1\n");
        }else if (diceRoll <= 49){
            lootItem = ("Slippers of spiderclimbing\n");
        }else if (diceRoll <= 51){
            lootItem = ("Staff of the adder\n");
        }else if (diceRoll <= 53){
            lootItem = ("Staff of the python\n");
        }else if (diceRoll <= 55){
            lootItem = ("Sword of vengence\n");
        }else if (diceRoll <= 57){
            lootItem = ("Trident of fish command\n");
        }else if (diceRoll <= 59){
            lootItem = ("Wand of magic missiles\n");
        }else if (diceRoll <= 61){
            lootItem = ("Wand of the war mage, +1\n");
        }else if (diceRoll <= 63){
            lootItem = ("Wand of web\n");
        }else if (diceRoll <= 65){
            lootItem = ("Bweapon of warningn");
        }else if (diceRoll <= 66){
            lootItem = ("Adamantine armor (chain mail)\n");
        }else if (diceRoll <= 67){
            lootItem = ("Adamantine armor (chain shirt)\n");
        }else if (diceRoll <= 68){
            lootItem = ("SAdamantine armor (scale mail)\n");
        }else if (diceRoll <= 69){
            lootItem = ("Bag of tricks (grey)\n");
        }else if (diceRoll <= 70){
            lootItem = ("Bag of tricks (rust)\n");
        }else if (diceRoll <= 71){
            lootItem = ("Bag of tricks (tan)\n");
        }else if (diceRoll <= 72){
            lootItem = ("Boots of winterlands\n");
        }else if (diceRoll <= 73){
            lootItem = ("Circlet of blasting\n");
        }else if (diceRoll <= 74){
            lootItem = ("Deck of illusions\n");
        }else if (diceRoll <= 75){
            lootItem = ("Eversmoking bottle\n");
        }else if (diceRoll <= 76){
            lootItem = ("Eyes of charming\n");
        }else if (diceRoll <= 77){
            lootItem = ("Eyes of the eagle\n");
        }else if (diceRoll <= 78){
            lootItem = ("Figureine of wonderous power (silver raven)\n");
        }else if (diceRoll <= 79){
            lootItem = ("Gem of brightness\n");
        }else if (diceRoll <= 80){
            lootItem = ("Gloves of missile snaring\n");
        }else if (diceRoll <= 81){
            lootItem = ("Gloves of swimming and climbing\n");
        }else if (diceRoll <= 82){
            lootItem = ("Gloves of thievery\n");
        }else if (diceRoll <= 83){
            lootItem = ("Headband of intellect\n");
        }else if (diceRoll <= 84){
            lootItem = ("Helm of telepathy\n");
        }else if (diceRoll <= 85){
            lootItem = ("Instrument of the bards (Doss lute)\n");
        }else if (diceRoll <= 86){
            lootItem = ("Instrument of the bards (Fochlucan bandore)\n");
        }else if (diceRoll <= 87){
            lootItem = ("Instrument of the bards (Mac-Fuimdh cittern)\n");
        }else if (diceRoll <= 88){
            lootItem = ("Medalion of thoughts\n");
        }else if (diceRoll <= 89){
            lootItem = ("Necklace of adaptation\n");
        }else if (diceRoll <= 90){
            lootItem = ("Periaptt of wound closure\n");
        }else if (diceRoll <= 91){
            lootItem = ("Pipes of haunting\n");
        }else if (diceRoll <= 92){
            lootItem = ("Pipes of the sewers\n");
        }else if (diceRoll <= 93){
            lootItem = ("Ring of jumping\n");
        }else if (diceRoll <= 94){
            lootItem = ("Ring of mind shielding\n");
        }else if (diceRoll <= 95){
            lootItem = ("Ring of warmth\n");
        }else if (diceRoll <= 96){
            lootItem = ("Ring of water walking\n");
        }else if (diceRoll <= 97){
            lootItem = ("Quiver of Ehlonna\n");
        }else if (diceRoll <= 98){
            lootItem = ("Stone of good luck\n");
        }else if (diceRoll <= 99){
            lootItem = ("Wind fan\n");
        }else{
            lootItem = ("Winged boots\n");
        }
        
        
        return lootItem;
    }
}
