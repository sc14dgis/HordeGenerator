/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class CR17P {
    String returnLoot(){
        diceRoller diceRoller = new diceRoller();
        int roll = diceRoller.rollDie(100);
        
        int GP = 0;
        for(int i=0; i<12; i++){GP = GP + diceRoller.rollDie(6); }
        GP = GP*1000;
        
        int PP = 0;
        for(int i=0; i<8; i++){PP = PP + diceRoller.rollDie(6); }
        PP = PP*1000;
        
        String itemString = "";
        if (roll <=2){
            // Nothing here unlucky
            itemString = "No items\n";
        }else if(roll <=5){
            GP = GP+10000;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(8);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=8){
            GP = GP+12500;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(8);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=11){
            GP = GP+15000;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(8);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=14){
            GP = GP+20000;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(8);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=22){
            GP = GP+10000;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=30){
            GP = GP+12500;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=38){
            GP = GP+15000;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=46){
            GP = GP+20000;
            magicItemTableD tableD = new magicItemTableD();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableD.getLoot();}
        }else if(roll <=52){
            GP = GP+10000;
            magicItemTableE tableE = new magicItemTableE();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableE.getLoot();}
        }else if(roll <=58){
            GP = GP+12500;
            magicItemTableE tableE = new magicItemTableE();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableE.getLoot();}
        }else if(roll <=63){
            GP = GP+15000;
            magicItemTableE tableE = new magicItemTableE();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableE.getLoot();}
        }else if(roll <=68){
            GP = GP+20000;
            magicItemTableE tableE = new magicItemTableE();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableE.getLoot();}
        }else if(roll <=69){
            GP = GP+10000;
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=70){
            GP = GP+12500;
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=71){
            GP = GP+15000;
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=72){
            GP = GP+20000;
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if(roll <=74){
            GP = GP+10000;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=76){
            GP = GP+12500;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=78){
            GP = GP+15000;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=80){
            GP = GP+20000;
            magicItemTableH tableH = new magicItemTableH();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableH.getLoot();}
        }else if(roll <=85){
            GP = GP+10000;
            magicItemTableI tableI = new magicItemTableI();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableI.getLoot();}
        }else if(roll <=90){
            GP = GP+12500;
            magicItemTableI tableI = new magicItemTableI();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableI.getLoot();}
        }else if(roll <=95){
            GP = GP+15000;
            magicItemTableI tableI = new magicItemTableI();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableI.getLoot();}
        }else {
            GP = GP+10000;
            magicItemTableI tableI = new magicItemTableI();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableI.getLoot();}
        }
        
        String moneyString = GP+"g  "+ PP+"p\n";
        String lootString = moneyString+itemString;
        return lootString;
    }
}
