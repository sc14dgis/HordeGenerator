/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableI {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem ;
        
        if (diceRoll <=5){
            lootItem = "Defender\n";
        }else if (diceRoll <= 10){
            lootItem = ("Hammer  of  thunderbolts\n");
        }else if (diceRoll <= 15){
            lootItem = ("Luck  blade\n");
        }else if (diceRoll <= 20){
            lootItem = ("Sword  of  answering\n");
        }else if (diceRoll <= 23){
            lootItem = ("Holy  avenger\n");
        }else if (diceRoll <= 26){
            lootItem = ("Ring  of  djinni summoning\n");
        }else if (diceRoll <= 29){
            lootItem = ("Ring  of  invisibility\n");
        }else if (diceRoll <= 32){
            lootItem = ("Ring  of  spell turning\n");
        }else if (diceRoll <= 35){
            lootItem = ("Rod  of  lordly might\n");
        }else if (diceRoll <= 38){
            lootItem = ("Staff  of  the magi\n");
        }else if (diceRoll <= 41){
            lootItem = ("Vorpal sword\n");
        }else if (diceRoll <= 43){
            lootItem = ("Belt  of  cloud giant strength\n");
        }else if (diceRoll <= 45){
            lootItem = ("Armor,  +2  breastplate\n");
        }else if (diceRoll <= 47){
            lootItem = ("Armor,  +3  chain  mail\n");
        }else if (diceRoll <= 49){
            lootItem = ("Armor,  +3  chain shirt\n");
        }else if (diceRoll <= 51){
            lootItem = ("Cloak  of  invisibility\n");
        }else if (diceRoll <= 53){
            lootItem = ("Crystal  ball  (legendary version)\n");
        }else if (diceRoll <= 55){
            lootItem = ("Armor, + 1 half plate\n");
        }else if (diceRoll <= 57){
            lootItem = ("Iron  flask\n");
        }else if (diceRoll <= 59){
            lootItem = ("Armor,  +3  leather\n");
        }else if (diceRoll <= 61){
            lootItem = ("Armor,  +1  plate\n");
        }else if (diceRoll <= 63){
            lootItem = ("Robe  of  the archmagi\n");
        }else if (diceRoll <= 65){
            lootItem = ("Rod  of  resurrection\n");
        }else if (diceRoll <= 67){
            lootItem = ("Armor,  +1  scale mail\n");
        }else if (diceRoll <= 69){
            lootItem = ("Scarab  of  protection\n");
        }else if (diceRoll <= 71){
            lootItem = ("Armor,  +2  splint\n");
        }else if (diceRoll <= 73){
            lootItem = ("Armor,  +2  studded leather\n");
        }else if (diceRoll <= 75){
            lootItem = ("Well  of  many worlds\n");
        }else if (diceRoll <= 76){
            String itemString;
            switch (diceRoller.rollDie(12)){
                case 1  :   itemString = "Armor,  +2  half plate";
                        break;
                case 2  :   itemString = "Armor,  +2  half plate" ;
                        break;
                case 3  :   itemString = "Armor,  +2  plate" ;
                        break;
                case 4  :   itemString = "Armor,  +2  plate" ;
                        break;
                case 5  :   itemString = "Armor,  +3  studded leather" ;
                        break;
                case 6  :   itemString = "Armor,  +3  studded leather" ;
                        break;
                case 7  :   itemString = "Armor,  +3  breastplate" ;
                        break;
                case 8  :   itemString = "Armor,  +3  breastplate" ;
                        break;
                case 9  :   itemString = "Armor,  +3  splint" ;
                        break;
                case 10  :   itemString = "Armor,  +3  splint" ;
                        break;
                case 11  :   itemString = "Armor,  +3  half plate" ;
                        break;
                default: itemString = "Armor,  +3  plate";
                        break;
            }
            lootItem = ("Magic armor ("+ itemString +") \n");
        }else if (diceRoll <= 77){
            lootItem = ("Apparatus  of  Kwalish\n");
        }else if (diceRoll <= 78){
            lootItem = ("Armor of invulnerability\n");
        }else if (diceRoll <= 79){
            lootItem = ("Belt  of  storm giant strength\n");
        }else if (diceRoll <= 80){
            lootItem = ("Cubic gate\n");
        }else if (diceRoll <= 81){
            lootItem = ("Deck  of  many things\n");
        }else if (diceRoll <= 82){
            lootItem = ("Efreeti chain\n");
        }else if (diceRoll <= 83){
            lootItem = ("Armor  of  resistance (half plate)\n");
        }else if (diceRoll <= 84){
            lootItem = ("Horn ofValhalla (iron)\n");
        }else if (diceRoll <= 85){
            lootItem = ("Instrument  of  the bards (OIIamh harp)\n");
        }else if (diceRoll <= 86){
            lootItem = ("loun stone (greater absorption)\n");
        }else if (diceRoll <= 87){
            lootItem = ("loun stone (mastery)\n");
        }else if (diceRoll <= 88){
            lootItem = ("loun stone (regeneration)\n");
        }else if (diceRoll <= 89){
            lootItem = ("Plate armor  of  etherealness\n");
        }else if (diceRoll <= 90){
            lootItem = ("Plate armor  of  resistance\n");
        }else if (diceRoll <= 91){
            lootItem = ("Ring  of air elemental command\n");
        }else if (diceRoll <= 92){
            lootItem = ("Ring  of earth elemental command\n");
        }else if (diceRoll <= 93){
            lootItem = ("Ring  of  fire  elemental command\n");
        }else if (diceRoll <= 94){
            lootItem = ("Ring  of  three wishes\n");
        }else if (diceRoll <= 95){
            lootItem = ("Ring  of  water elemental command\n");
        }else if (diceRoll <= 96){
            lootItem = ("Sphere  of  annihilation\n");
        }else if (diceRoll <= 97){
            lootItem = ("Talisman  of  pure good\n");
        }else if (diceRoll <= 98){
            lootItem = ("Talisman  of  the sphere\n");
        }else if (diceRoll <= 99){
            lootItem = ("Talisman  of  ultimate  evil\n");
        }else{
            lootItem = ("Tome  of  the stilled tongue\n");
        }
        
        
        return lootItem;
    }
}
