/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableC {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem = "";
        
        if (diceRoll <=15){
            lootItem = "Potion of superior healing\n";
        }else if (diceRoll <= 22){
            lootItem = ("Spell scroll(4th level)\n");
        }else if (diceRoll <= 27){
            lootItem = ("Amunition, +2\n");
        }else if (diceRoll <= 32){
            lootItem = ("Potion of clarvoyance\n");
        }else if (diceRoll <= 37){
            lootItem = ("Potion of diminution\n");
        }else if (diceRoll <= 42){
            lootItem = ("Potion of gaseous form\n");
        }else if (diceRoll <= 47){
            lootItem = ("Potion of frost giant strength\n");
        }else if (diceRoll <= 52){
            lootItem = ("Potion of stone giant strength\n");
        }else if (diceRoll <= 57){
            lootItem = ("Potion of heroism\n");
        }else if (diceRoll <= 62){
            lootItem = ("Potion of invulnerability\n");
        }else if (diceRoll <= 67){
            lootItem = ("Potion of mind reading\n");
        }else if (diceRoll <= 72){
            lootItem = ("Spell scroll (5th level)\n");
        }else if (diceRoll <= 75){
            lootItem = ("Elixer of health\n");
        }else if (diceRoll <= 78){
            lootItem = ("Oil of eatheralness\n");
        }else if (diceRoll <= 81){
            lootItem = ("Potion of fire giant strength\n");
        }else if (diceRoll <= 84){
            lootItem = ("Quaal's feather token\n");
        }else if (diceRoll <= 87){
            lootItem = ("Scroll of protection\n");
        }else if (diceRoll <= 89){
            lootItem = ("Bag of beans\n");
        }else if (diceRoll <= 91){
            lootItem = ("Bead of force\n");
        }else if (diceRoll == 92){
            lootItem = ("Chime of opening\n");
        }else if (diceRoll == 93){
            lootItem = ("Decanter of endless water\n");
        }else if (diceRoll == 94){
            lootItem = ("Eyes of minute seeing\n");
        }else if (diceRoll == 95){
            lootItem = ("Folding boat\n");
        }else if (diceRoll == 96){
            lootItem = ("Heward's handy haversack\n");
        }else if (diceRoll == 97){
            lootItem = ("Horseshoes of speed\n");
        }else if (diceRoll == 98){
            lootItem = ("Necklace of fireballs\n");
        }else if (diceRoll == 99){
            lootItem = ("Periapt of health\n");
        }else{
            lootItem = ("Sending stones\n");
        }
        
        
        return lootItem;
    }
}
