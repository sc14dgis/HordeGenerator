/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableE {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem = "";
        
        if (diceRoll <=30){
            lootItem = "Spell scroll (8th level)\n";
        }else if (diceRoll <= 55){
            lootItem = ("Potion of storm giant strength\n");
        }else if (diceRoll <= 70){
            lootItem = ("Potion supreme healing\n");
        }else if (diceRoll <= 85){
            lootItem = ("Spell scroll (9th level)\n");
        }else if (diceRoll <= 93){
            lootItem = ("Universal solvent\n");
        }else if (diceRoll <= 98){
            lootItem = ("Arrow of slaying\n");
        }else{
            lootItem = ("Sovereign glue\n");
        }
        
        
        return lootItem;
    }
}
