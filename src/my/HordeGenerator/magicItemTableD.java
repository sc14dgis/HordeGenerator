/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableD {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem = "";
        
        if (diceRoll <=20){
            lootItem = "Potion of supreme healing\n";
        }else if (diceRoll <= 30){
            lootItem = ("Potion of invisability\n");
        }else if (diceRoll <= 40){
            lootItem = ("Potion of speed\n");
        }else if (diceRoll <= 50){
            lootItem = ("Spell scroll (6th level)\n");
        }else if (diceRoll <= 57){
            lootItem = ("Spell scroll (7th level)\n");
        }else if (diceRoll <= 62){
            lootItem = ("Amiunition, +3\n");
        }else if (diceRoll <= 67){
            lootItem = ("Oil of sharpness\n");
        }else if (diceRoll <= 72){
            lootItem = ("Potion of flying\n");
        }else if (diceRoll <= 77){
            lootItem = ("Potion of cloud giant strength\n");
        }else if (diceRoll <= 82){
            lootItem = ("Potion of longevity\n");
        }else if (diceRoll <= 87){
            lootItem = ("Potion of vitality\n");
        }else if (diceRoll <= 92){
            lootItem = ("Spell scroll (8th level)\n");
        }else if (diceRoll <= 95){
            lootItem = ("Horseshoes of a zephyr\n");
        }else if (diceRoll <= 98){
            lootItem = ("Nolzur's marvelous pigments\n");
        }else if (diceRoll == 99){
            lootItem = ("Bag of devouring\n");
        }else{
            lootItem = ("Portable hole\n");
        }
        
        
        return lootItem;
    }
}
