/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class CR10 {
    String returnLoot(){
        diceRoller diceRoller = new diceRoller();
        int roll = diceRoller.rollDie(100);
        int CP = 0;
        for(int i=0; i<2; i++){ 
            CP = CP + diceRoller.rollDie(6); 
        }
        CP = CP*100;
                
        int SP = 0;
        for(int i=0; i<2; i++){SP = SP + diceRoller.rollDie(6); }
        SP = SP*1000;
        
        int GP = 0;
        for(int i=0; i<6; i++){GP = GP + diceRoller.rollDie(6); }
        GP = GP*100;
        
        int PP = 0;
        for(int i=0; i<3; i++){PP = PP + diceRoller.rollDie(6); }
        PP = PP*10;
        
        String itemString = "";
        if (roll <=4){
            // Nothing here unlucky
        }else if(roll <=10){
            GP = GP+125;
            itemString = "No items\n";
        }else if(roll <=16){
            GP = GP+500;
            itemString = "No items\n";
        }else if(roll <=22){
            GP = GP+1000;
            itemString = "No items\n";
        }else if(roll <=28){
            GP = GP+1250;
            itemString = "No items\n";
        }else if(roll <=32){
            GP = GP+125;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableA.getLoot();}
        }else if(roll <=36){
            GP = GP+500;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableA.getLoot();}
        }else if(roll <=40){
            GP = GP+1000;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableA.getLoot();}
        }else if(roll <=44){
            GP = GP+1250;
            magicItemTableA tableA = new magicItemTableA();
            for (int i = 0 ; i<diceRoller.rollDie(6);i++){itemString = itemString + tableA.getLoot();}
        }else if(roll <=49){
            GP = GP+125;
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=54){
            GP = GP+500;
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=59){
            GP = GP+1000;
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=63){
            GP = GP+1250;
            magicItemTableB tableB = new magicItemTableB();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableB.getLoot();}
        }else if(roll <=66){
            GP = GP+125;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if(roll <=69){
            GP = GP+500;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if (roll <=72){
            GP = GP+1000;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if (roll <=74){
            GP = GP+1000;
            magicItemTableC tableC = new magicItemTableC();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableC.getLoot();}
        }else if (roll <=76){
            GP = GP+125;
            magicItemTableD tableD = new magicItemTableD();
            itemString = itemString + tableD.getLoot();
        }else if (roll <=78){
            GP = GP+500;
            magicItemTableD tableD = new magicItemTableD();
            itemString = itemString + tableD.getLoot();
        }else if (roll <=79){
            GP = GP+1000;
            magicItemTableD tableD = new magicItemTableD();
            itemString = itemString + tableD.getLoot();
        }else if (roll <=80){
            GP = GP+1000;
            magicItemTableD tableD = new magicItemTableD();
            itemString = itemString + tableD.getLoot();
        }else if (roll <=84){
            GP = GP+1000;
            magicItemTableF tableF = new magicItemTableF();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableF.getLoot();}
        }else if (roll <=88){
            GP = GP+500;
            magicItemTableF tableF = new magicItemTableF();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableF.getLoot();}
        }else if (roll <=91){
            GP = GP+1000;
            magicItemTableF tableF = new magicItemTableF();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableF.getLoot();}
        }else if (roll <=94){
            GP = GP+1250;
            magicItemTableF tableF = new magicItemTableF();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableF.getLoot();}
        }else if (roll <=96){
            GP = GP+1000;
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if (roll <=98){
            GP = GP+1250;
            magicItemTableG tableG = new magicItemTableG();
            for (int i = 0 ; i<diceRoller.rollDie(4);i++){itemString = itemString + tableG.getLoot();}
        }else if (roll <=99){
            GP = GP+1000;
            magicItemTableH tableH = new magicItemTableH();
            itemString = itemString + tableH.getLoot();
        }else {
            GP = GP+350;
            magicItemTableH tableH = new magicItemTableH();
            itemString = itemString + tableH.getLoot();
        }
        
        String moneyString = CP+"c  "+ SP+"s  "+ GP+"g  "+ PP+"p\n";
        String lootString = moneyString+itemString;
        return lootString;
    }
}
