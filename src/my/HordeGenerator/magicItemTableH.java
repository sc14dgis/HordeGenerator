/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.HordeGenerator;

/**
 *
 * @author Daniel
 */
public class magicItemTableH {
    String getLoot(){
        diceRoller diceRoller = new diceRoller();
        int diceRoll = diceRoller.rollDie(100);
        
        String lootItem = "";
        
        if (diceRoll <=10){
            lootItem = "Weapon,  +3\n";
        }else if (diceRoll <= 12){
            lootItem = ("Amulet  of  the planes\n");
        }else if (diceRoll <= 14){
            lootItem = ("Carpet  of  flying\n");
        }else if (diceRoll <= 16){
            lootItem = ("Crystal  ball  (very rare version)\n");
        }else if (diceRoll <= 18){
            lootItem = ("Ring  of  regeneration\n");
        }else if (diceRoll <= 20){
            lootItem = ("Ring  of  shooting stars\n");
        }else if (diceRoll <= 22){
            lootItem = ("Ring  of telekinesis\n");
        }else if (diceRoll <= 24){
            lootItem = ("Robe of scintillating colors\n");
        }else if (diceRoll <= 26){
            lootItem = ("Robe  of  stars\n");
        }else if (diceRoll <= 28){
            lootItem = ("Rod  of  absorption\n");
        }else if (diceRoll <= 30){
            lootItem = ("Rod  of  alertness\n");
        }else if (diceRoll <= 32){
            lootItem = ("Rod  of  security\n");
        }else if (diceRoll <= 34){
            lootItem = ("Rod  of  the pact keeper,  +3\n");
        }else if (diceRoll <= 36){
            lootItem = ("Scimitar  of  speed\n");
        }else if (diceRoll <= 38){
            lootItem = ("Shield,  +3\n");
        }else if (diceRoll <= 40){
            lootItem = ("Staff of  fire\n");
        }else if (diceRoll <= 42){
            lootItem = ("Staff  of  frost\n");
        }else if (diceRoll <= 44){
            lootItem = ("Staff  of  power\n");
        }else if (diceRoll <= 46){
            lootItem = ("Staff  of  striking\n");
        }else if (diceRoll <= 48){
            lootItem = ("Staff  of  thunder and lightning\n");
        }else if (diceRoll <= 50){
            lootItem = ("Sword  of  sharpness\n");
        }else if (diceRoll <= 52){
            lootItem = ("Wand  of  polymorph\n");
        }else if (diceRoll <= 54){
            lootItem = ("Wand  of  the war mage, +3\n");
        }else if (diceRoll <= 55){
            lootItem = ("Adamantine armor (half plate)\n");
        }else if (diceRoll <= 56){
            lootItem = ("Adamantine armor (plate)\n");
        }else if (diceRoll <= 57){
            lootItem = ("Animated shield\n");
        }else if (diceRoll <= 58){
            lootItem = ("Belt  of  fire  giant strength\n");
        }else if (diceRoll <= 59){
            lootItem = ("Belt  of  frost (or stone) giant strength\n");
        }else if (diceRoll <= 60){
            lootItem = ("Armor, + 1 breastplate\n");
        }else if (diceRoll <= 61){
            lootItem = ("Armor  of  resistance (breastplate)\n");
        }else if (diceRoll <= 62){
            lootItem = ("Candle  of  invocation\n");
        }else if (diceRoll <= 63){
            lootItem = ("Armor,  +2  chain mail\n");
        }else if (diceRoll <= 64){
            lootItem = ("Armor,  +2  chain shirt\n");
        }else if (diceRoll <= 65){
            lootItem = ("Cloak  of  arachnida\n");
        }else if (diceRoll <= 66){
            lootItem = ("Dancing sword\n");
        }else if (diceRoll <= 67){
            lootItem = ("Demon armor\n");
        }else if (diceRoll <= 68){
            lootItem = ("Dragon scale mail\n");
        }else if (diceRoll <= 69){
            lootItem = ("Dwarven plate\n");
        }else if (diceRoll <= 70){
            lootItem = ("Dwarven thrower\n");
        }else if (diceRoll <= 71){
            lootItem = ("Efreeti bottle\n");
        }else if (diceRoll <= 72){
            lootItem = ("Figurine  of  wondrous power (obsidian steed)\n");
        }else if (diceRoll <= 73){
            lootItem = ("Frost brand\n");
        }else if (diceRoll <= 74){
            lootItem = ("Helm  of  brilliance\n");
        }else if (diceRoll <= 75){
            lootItem = ("Horn ofValhalla (bronze)\n");
        }else if (diceRoll <= 76){
            lootItem = ("Instrument of the bards (Anstruth harp)\n");
        }else if (diceRoll <= 77){
            lootItem = ("loun stone (absorption)\n");
        }else if (diceRoll <= 78){
            lootItem = ("loun stone (agility)\n");
        }else if (diceRoll <= 79){
            lootItem = ("loun stone (fortitude)\n");
        }else if (diceRoll <= 80){
            lootItem = ("loun stone (insight)\n");
        }else if (diceRoll <= 81){
            lootItem = ("loun stone (intellect)\n");
        }else if (diceRoll <= 82){
            lootItem = ("loun  stone  (leadership)\n");
        }else if (diceRoll <= 83){
            lootItem = ("loun  stone  (strength)\n");
        }else if (diceRoll <= 84){
            lootItem = ("Armor, +2 leather\n");
        }else if (diceRoll <= 85){
            lootItem = ("Manual  of  bodily health\n");
        }else if (diceRoll <= 86){
            lootItem = ("Manual  of  gainful exercise\n");
        }else if (diceRoll <= 87){
            lootItem = ("Manual  of  golems\n");
        }else if (diceRoll <= 88){
            lootItem = ("Manual  of  quickness  of  action\n");
        }else if (diceRoll <= 89){
            lootItem = ("Manual  of  quickness  of  action\n");
        }else if (diceRoll <= 90){
            lootItem = ("Nine  lives stealer\n");
        }else if (diceRoll <= 91){
            lootItem = ("Oath bow\n");
        }else if (diceRoll <= 92){
            lootItem = ("Armor,  +2  scale mail\n");
        }else if (diceRoll <= 93){
            lootItem = ("Spellguard shield\n");
        }else if (diceRoll <= 94){
            lootItem = ("Armor, + 1 splint\n");
        }else if (diceRoll <= 95){
            lootItem = ("Armor  of  resistance (splint)\n");
        }else if (diceRoll <= 96){
            lootItem = ("Armor, + 1 studded leather\n");
        }else if (diceRoll <= 97){
            lootItem = ("Armor  of  resistance (studded leather)\n");
        }else if (diceRoll <= 98){
            lootItem = ("Tome  of  clear thought\n");
        }else if (diceRoll <= 99){
            lootItem = ("Tome  of  leadership and influence\n");
        }else{
            lootItem = ("Tome  of  understanding\n");
        }
        
        
        return lootItem;
    }
}
